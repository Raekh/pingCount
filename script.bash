#!/bin/bash
curl -sSi localhost:3000/ping > /dev/null
countFil="$(cat count)"
countTest="$(curl -sSi localhost:3000/count | grep pingCount | cut -d \" -f 4)"
if [[ countFil -eq countTest ]]
then
echo Initial count : ok. \(got and expected $countFil\)
else
echo Initial count : not ok \(got $countTest and expected $countFil\)
fi
curl -sSi localhost:3000/ping >> /dev/null
echo Pinged one more time.
countFil="$(cat count)"
countTest="$(curl -sSi localhost:3000/count | grep pingCount | cut -d \" -f 4)"
if [[ countFil -eq countTest ]]
then
echo Second count : ok. \(got and expected $countFil\)
else
echo Second count : not ok \(got $countTest and expected $countFil\)
fi

