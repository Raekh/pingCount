# Server Node.js
# Utilisation

Rendre tous les bash éxécutables
  ```bash
  chmod +x *.bash
  ```
- Si npm, node, raven et jq sont installés, lancer `configRaven.bash`
- Sinon, lancer `install.bash`

Pour tester le server manuellement : `node server.js` et tester les routes `http://localhost:3000/[ping/count]`
 à l'aide de curl
Sinon, lancer `script.bash` qui fera la batterie de tests.