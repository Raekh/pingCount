#!/bin/bash
sudo apt-get install node -y > /dev/null
sudo apt-get install nodejs-legacy -y > /dev/null
sudo apt-get install jq -y > /dev/null
sudo apt-get install raven --save -y > /dev/null
sudo apt-get install npm -y > /dev/null
sudo npm install raven --save > /dev/null
if [ ! -f ./dsnconf ]; then
    echo "Raven has not been configured. Please input a valid DSN."
    read dsn
    echo $dsn > ./dsnconf
fi
 
