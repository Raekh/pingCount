const http = require('http')
const port = 3000
const fs = require('fs')
var Raven = require('raven');

fs.readFile('dsnconf','utf8', function(err,data){
  if(err){
    Raven.captureException(err);
    console.log("Raven is not configured. Run the install script to get started.")
    proccess.exit(1)
  }
  console.log("DSN : "+data)
  Raven.config('https://'+data+'@sentry.io/257005').install()
})
const requestHandler = (request, response) => {
  // console.log(request.url)
  fs.readFile('count','utf8', function(err,data){
    var num
    if(err){
      fs.writeFileSync('count','0')
      num = fs.readFileSync('count','utf8')
    }
    else{
      num = data
    }
    if(request.url == "/ping"){
      var newNum = parseInt(num)+1;
      fs.writeFileSync('count',newNum);
      response.setHeader('Content-Type','application/json')
      response.end(JSON.stringify({message: 'pong'}))
    }

    else if(request.url == "/count"){
      response.setHeader('Content-Type', 'application/json')
      response.end(JSON.stringify({pingCount: num}))
    }

    else{
      response.setHeader('Content-Type', 'application/json')
      response.end(JSON.stringify({error: 404}))
    }
  })
}

const server = http.createServer(requestHandler)

server.listen(port,(err)=>{
  if(err){
    return console.log('Something bad happened...',err)
  }
  console.log(`Server is listening on ${port}`)
})	 
